package com.company;

import com.company.interpreter.InterpreterVisitor;
import com.company.interpreter.entity.LineEntity;
import com.company.interpreter.exceptions.*;
import com.company.lexer.Lexer;
import com.company.lexer.Token;
import com.company.parser.Parser;
import com.company.parser.exceptions.UnexpectedTokenException;
import com.company.visitor.PrintVisitor;
import com.company.walker.BaseWalker;
import com.company.walker.PrintWalker;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Main {

    public static void main(String[] args) throws IOException, UnexpectedTokenException, ParametersNumberException, ValueOutOfBoundsException, DuplicateIdentifierException, UnexpectedParameterException, UnknownFunctionException, UnknownIdentifierException {
        var path = Paths.get("example2.txt");
        String source = Files.readAllLines(path).stream().reduce((l, acc) -> acc = l + "\n" + acc).get();

        Lexer lexer = new Lexer(source);
        List<Token> tokens = lexer.scanTokens();

        // tokens.forEach(System.out::println);
        // System.out.println();

        var parser = new Parser(tokens);

        // var walker = new PrintWalker(parser.tree, new PrintVisitor());
        var walker = new BaseWalker(parser.tree, new InterpreterVisitor());

        try {
            walker.walk();
        } catch (Exception e) {
            throw e;
            //System.out.println(e.getLocalizedMessage());
        }

    }

}

