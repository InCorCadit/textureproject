package com.company.walker;

import com.company.interpreter.exceptions.*;
import com.company.parser.Rule;
import com.company.parser.SyntaxTree;
import com.company.visitor.Visitor;

import java.io.IOException;

public class BaseWalker {
    protected final SyntaxTree<Rule> tree;
    protected final Visitor visitor;

    public BaseWalker(SyntaxTree<Rule> tree, Visitor visitor) {
        this.tree = tree;
        this.visitor = visitor;
    }

    public void walk() throws DuplicateIdentifierException, UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException {
        tree.root.value.accept(visitor);

        for (var node : tree.root.children) {
            walkChild(node);
        }
    }

    protected void walkChild(SyntaxTree.Node<Rule> node) throws DuplicateIdentifierException, UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException {
        node.value.accept(visitor);

        for (var subnode : node.children) {
            walkChild(subnode);
        }
    }
}
