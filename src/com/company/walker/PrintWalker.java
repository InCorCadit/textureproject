package com.company.walker;

import com.company.interpreter.exceptions.*;
import com.company.parser.Parser;
import com.company.parser.Rule;
import com.company.parser.SyntaxTree;
import com.company.visitor.Visitor;

import java.io.IOException;

public class PrintWalker extends BaseWalker {

    public PrintWalker(SyntaxTree<Rule> tree, Visitor visitor) {
        super(tree, visitor);
    }


    public void walk() throws DuplicateIdentifierException, UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException {
        tree.root.value.accept(visitor);

        System.out.print(" (\n");

        for (var node : tree.root.children) {
            walkChild(node);

            if (tree.root.children.indexOf(node) != tree.root.children.size() - 1) {
                System.out.print(" ");
            }

            if (node.value.getClass().equals(Parser.EOL.class)) {
                System.out.print('\n');
            }
        }

        System.out.print(")");
    }

    @Override
    protected void walkChild(SyntaxTree.Node<Rule> node) throws DuplicateIdentifierException, UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException {
        node.value.accept(visitor);

        if (node.children.size() > 0) {
            System.out.print(" (");

            for (var subnode : node.children) {
                walkChild(subnode);

                if (node.children.indexOf(subnode) != node.children.size() - 1) {
                    System.out.print(" ");
                }
            }

            System.out.print(")");
        }
    }
}
