package com.company.parser.exceptions;

public class UnexpectedTokenException extends Exception {

    public UnexpectedTokenException(String expected, int line) {
        super(String.format("Unexpected Token error: expected %s at line %d", expected, line));
    }

    public UnexpectedTokenException(int line) {
        super(String.format("Unexpected Token error: at line %d", line));
    }

}
