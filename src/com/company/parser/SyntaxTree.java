package com.company.parser;

import java.util.ArrayList;
import java.util.List;

public class SyntaxTree<T> {
    public Node<T> root;

    public static class Node<T> {
        public T value;
        public Node<T> parent;
        public List<Node<T>> children;

        public Node(T value, Node<T>parent) {
            this.value = value;
            this.parent = parent;
            this.children = new ArrayList<>();
        }
    }
}
