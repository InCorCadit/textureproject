package com.company.parser;

import com.company.interpreter.exceptions.*;
import com.company.visitor.Visitor;

import java.io.IOException;

public abstract class Rule {
    public boolean visited = false;

    public abstract void accept(Visitor visitor) throws DuplicateIdentifierException, UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException;

}
