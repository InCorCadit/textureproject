package com.company.parser;

import com.company.interpreter.exceptions.*;
import com.company.interpreter.function.FunctionCall;
import com.company.lexer.Token;
import com.company.parser.exceptions.UnexpectedTokenException;
import com.company.visitor.Visitor;

import java.io.IOException;
import java.util.*;

import static com.company.lexer.Token.TokenType.*;

public class Parser {
    public SyntaxTree<Rule> tree;

    private final Queue<Token> tokens;
    private final Stack<SyntaxTree.Node<Rule>> ruleStack;
    private Token token;

    public Parser(List<Token> tokens) throws UnexpectedTokenException {
        this.tokens = new LinkedList<>();
        this.tokens.addAll(tokens);

        this.tree = new SyntaxTree<>();
        this.ruleStack = new Stack<>();

        nextToken();
        sourceCode();
    }

    // Utility
    private void nextToken() {
        token = tokens.poll();
    }

    private Token viewNext() { return tokens.peek(); }

    private SyntaxTree.Node<Rule> addChildToCurrentToken(Rule rule) {
        var parent = ruleStack.peek();
        var curNode = new SyntaxTree.Node<>(rule, parent);
        parent.children.add(curNode);
        return curNode;
    }

    // Rules
    public static class SourceCodeRule extends Rule {

        @Override
        public void accept(Visitor visitor) { visitor.visitSourceCodeRule(this); }
    }

    public void sourceCode() throws UnexpectedTokenException {
        var context = new SourceCodeRule();
        SyntaxTree.Node<Rule> curNode = new SyntaxTree.Node<>(context, null);

        tree.root = curNode;
        ruleStack.push(curNode);
        while (token != null) {
            switch (token.type) {
                case DEF -> {
                    definition();
                    eol();
                }
                case IDENTIFIER -> {
                    functionCall();
                    eol();
                }
                default -> throw new UnexpectedTokenException(token.line);
            }
        }

        ruleStack.pop();
    }

    public static class DefinitionRule extends Rule {
        IDENTIFIER variable;
        FunctionCallRule functionCall;
        LiteralRule literal;

        @Override
        public void accept(Visitor visitor) throws DuplicateIdentifierException, UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException { visitor.visitDefinitionRule(this); }

        public IDENTIFIER getVariable() {
            return variable;
        }

        public FunctionCallRule getFunctionCall() { return functionCall; }

        public LiteralRule getLiteral() { return literal; }
    }

    public void definition() throws UnexpectedTokenException {
        var context = new DefinitionRule();
        var curNode = addChildToCurrentToken(context);
        ruleStack.push(curNode);

        def();
        identifier(false);
        context.variable = (IDENTIFIER) curNode.children.get(curNode.children.size() - 1).value;
        assign();
        switch (token.type) {
            case IDENTIFIER -> {
                functionCall();
                context.functionCall = (FunctionCallRule) curNode.children.get(curNode.children.size() - 1).value;
            }
            case STRING, INTEGER -> {
                literal();
                context.literal = (LiteralRule) curNode.children.get(curNode.children.size() - 1).value;
            }
            default -> throw new UnexpectedTokenException(token.line);
        }

        ruleStack.pop();
    }

    public static class FunctionCallRule extends Rule {
        IDENTIFIER variable;
        IDENTIFIER function;
        List<ParameterRule> params;

        FunctionCallRule() {
            params = new ArrayList<>();
        }

        public IDENTIFIER getVariable() {
            return variable;
        }

        public IDENTIFIER getFunction() {
            return function;
        }

        public List<ParameterRule> getParams() {
            return params;
        }


        @Override
        public void accept(Visitor visitor) throws UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException { visitor.visitFunctionCallRule(this); }
    }

    public void functionCall() throws UnexpectedTokenException {
        var context = new FunctionCallRule();
        var curNode = addChildToCurrentToken(context);
        ruleStack.push(curNode);

        if (viewNext().type == DOT) {
            identifier(false);
            context.variable = (IDENTIFIER) curNode.children.get(curNode.children.size() - 1).value;
            dot();
        }

        identifier(true);
        context.function = (IDENTIFIER) curNode.children.get(curNode.children.size() - 1).value;
        semicolon();
        open_paren();
        while (token.type != CLOSE_PAREN) {
            parameter();
            context.params.add((ParameterRule) curNode.children.get(curNode.children.size() - 1).value);
            if (token.type == CLOSE_PAREN) break;
            comma();
            if (token.type == CLOSE_PAREN) throw new UnexpectedTokenException(")", token.line);
        }
        close_paren();

        ruleStack.pop();
    }

    public static class ParameterRule extends Rule {
        FunctionCallRule function;
        IDENTIFIER identifier;
        LiteralRule literal;

        @Override
        public void accept(Visitor visitor) throws UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException { visitor.visitParameterRule(this); }

        public FunctionCallRule getFunction() { return function; }

        public IDENTIFIER getVariable() { return identifier; }

        public LiteralRule getLiteral() { return literal; }
    }

    public void parameter() throws UnexpectedTokenException {
        var context = new ParameterRule();
        var curNode = addChildToCurrentToken(context);

        ruleStack.push(curNode);
        switch (token.type) {
            case IDENTIFIER -> {
                if (viewNext().type == SEMICOLON) {
                    functionCall();
                    context.function = (FunctionCallRule) curNode.children.get(curNode.children.size() - 1).value;
                }
                else {
                    identifier(false);
                    context.identifier = (IDENTIFIER) curNode.children.get(curNode.children.size() - 1).value;
                }
            }
            case STRING, INTEGER -> {
                literal();
                context.literal = (LiteralRule) curNode.children.get(curNode.children.size() - 1).value;
            }
            default -> throw new UnexpectedTokenException(token.line);
        }

        ruleStack.pop();
    }

    public static class LiteralRule extends Rule {
        INTEGER integer;
        STRING string;

        @Override
        public void accept(Visitor visitor) { visitor.visitLiteralRule(this); }

        public INTEGER getInteger() {
            return integer;
        }

        public STRING getString() {
            return string;
        }
    }

    public void literal() {
        var context = new LiteralRule();
        var curNode = addChildToCurrentToken(context);

        ruleStack.push(curNode);
        switch (token.type) {
            case INTEGER -> {
                integer();
                context.integer = (INTEGER) curNode.children.get(curNode.children.size() - 1).value;
            }
            case STRING -> {
                string();
                context.string = (STRING) curNode.children.get(curNode.children.size() - 1).value;
            }
        }

        ruleStack.pop();
    }

    public static class IDENTIFIER extends Rule {
        public final String name;
        public boolean isFunction;

        public IDENTIFIER(String name, boolean isFunction) {
            this.name = name;
            this.isFunction = isFunction;
        }

        @Override
        public void accept(Visitor visitor) { visitor.visitIDENTIFIER(this); }
    }

    public void identifier(boolean isFunction) throws UnexpectedTokenException {
        if (token.type != IDENTIFIER) throw new UnexpectedTokenException("identifier", token.line);
        addChildToCurrentToken(new IDENTIFIER(token.lexeme, isFunction));
        nextToken();
    }

    public static class INTEGER extends Rule {
        int value;

        INTEGER(int value) {
            this.value = value;
        }

        @Override
        public void accept(Visitor visitor) { visitor.visitINTEGER(this); }

        public int getValue() { return value; }
    }

    public void integer() {
        addChildToCurrentToken(new INTEGER((int) token.literal));
        nextToken();
    }

    public static class STRING extends Rule {
        String value;

        STRING(String value) {
            this.value = value;
        }

        @Override
        public void accept(Visitor visitor) { visitor.visitSTRING(this); }

        public String getValue() { return value; }
    }

    public void string() {
        addChildToCurrentToken(new STRING((String) token.literal));
        nextToken();
    }

    public static class DEF extends Rule {

        @Override
        public void accept(Visitor visitor) { visitor.visitDEF(this); }
    }

    public void def() throws UnexpectedTokenException {
        if (token.type != DEF) throw new UnexpectedTokenException("def", token.line);
        addChildToCurrentToken(new DEF());
        nextToken();
    }

    public static class COMMA extends Rule {
        @Override
        public void accept(Visitor visitor) { visitor.visitCOMMA(this); }
    }

    public void comma() throws UnexpectedTokenException {
        if (token.type != COMMA) throw new UnexpectedTokenException(",", token.line);
        addChildToCurrentToken(new COMMA());
        nextToken();
    }

    public static class DOT extends Rule {
        @Override
        public void accept(Visitor visitor) { visitor.visitDOT(this); }
    }

    public void dot() throws UnexpectedTokenException {
        if (token.type != DOT) throw new UnexpectedTokenException(".", token.line);
        addChildToCurrentToken(new DOT());
        nextToken();
    }

    public static class ASSIGN extends Rule {
        @Override
        public void accept(Visitor visitor) { visitor.visitASSIGN(this); }
    }

    public void assign() throws UnexpectedTokenException {
        if (token.type != ASSIGN) throw new UnexpectedTokenException("=", token.line);
        addChildToCurrentToken(new ASSIGN());
        nextToken();
    }

    public static class SEMICOLON extends Rule {
        @Override
        public void accept(Visitor visitor) { visitor.visitSEMICOLON(this); }
    }

    public void semicolon() throws UnexpectedTokenException {
        if (token.type != SEMICOLON) throw new UnexpectedTokenException(":", token.line);
        addChildToCurrentToken(new SEMICOLON());
        nextToken();
    }

    public static class OPEN_PAREN extends Rule {
        @Override
        public void accept(Visitor visitor) { visitor.visitOPEN_PAREN(this); }
    }

    public void open_paren() throws UnexpectedTokenException {
        if (token.type != OPEN_PAREN) throw new UnexpectedTokenException("(", token.line);
        addChildToCurrentToken(new OPEN_PAREN());
        nextToken();
    }

    public static class CLOSE_PAREN extends Rule {
        @Override
        public void accept(Visitor visitor) { visitor.visitCLOSE_PAREN(this); }
    }

    public void close_paren() throws UnexpectedTokenException {
        if (token.type != CLOSE_PAREN) throw new UnexpectedTokenException("(", token.line);
        addChildToCurrentToken(new CLOSE_PAREN());
        nextToken();
    }

    public static class EOL extends Rule {
        @Override
        public void accept(Visitor visitor) { visitor.visitEOL(this); }
    }

    public void eol() throws UnexpectedTokenException {
        if (token.type != EOL) throw new UnexpectedTokenException(";", token.line);
        addChildToCurrentToken(new EOL());
        nextToken();
    }

    public static class O_BR extends Rule {
        @Override
        public void accept(Visitor visitor) { visitor.visitO_BR(this); }
    }

    public static class C_BR extends Rule {
        @Override
        public void accept(Visitor visitor) { visitor.visitC_BR(this); }
    }
}
