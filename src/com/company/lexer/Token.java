package com.company.lexer;

public class Token {
    public final TokenType type;
    public final String lexeme;
    public final Object literal;
    public final int line;

    Token(TokenType type, String lexeme, Object literal, int line) {
        this.type = type;
        this.lexeme = lexeme;
        this.literal = literal;
        this.line = line;
    }

    public String toString() {
        return type + " " + lexeme + " " + literal;
    }

    public enum TokenType {
        DEF,
        DOT,
        COMMA,
        ASSIGN,
        SEMICOLON,
        OPEN_PAREN,
        CLOSE_PAREN,
        EOL,
        INTEGER,
        O_BR,
        C_BR,
        STRING,
        IDENTIFIER,
        UNDEFINED,
    }
}
