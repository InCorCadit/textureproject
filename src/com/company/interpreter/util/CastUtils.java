package com.company.interpreter.util;

public class CastUtils {

    public static boolean canAssign(Object obj, Class target) {
        return target.isAssignableFrom(obj.getClass());
    }
}
