package com.company.interpreter;

import com.company.interpreter.entity.IntegerEntity;
import com.company.interpreter.entity.StringEntity;
import com.company.interpreter.entity.base.Entity;
import com.company.interpreter.exceptions.*;
import com.company.interpreter.function.FunctionCall;
import com.company.parser.Parser;
import com.company.visitor.BaseVisitor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class InterpreterVisitor extends BaseVisitor {
    private Context context;

    @Override
    public void visitSourceCodeRule(Parser.SourceCodeRule rule) {
        context = new Context();
    }

    @Override
    public void visitDefinitionRule(Parser.DefinitionRule rule) throws DuplicateIdentifierException, UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException {
        if (rule.visited) {
            return;
        }

        String varName = rule.getVariable().name;

        if (rule.getLiteral() != null) {
            visitLiteralRule(rule.getLiteral());
        } else {
            visitFunctionCallRule(rule.getFunctionCall());
        }

        context.variablesTable.addVariable(varName, context.exposeBuffer());

        rule.visited = true;
    }

    @Override
    public void visitFunctionCallRule(Parser.FunctionCallRule rule) throws UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException {
        if (rule.visited) {
            return;
        }

        var call = new FunctionCall(rule.getFunction().name, rule.getVariable() != null ? rule.getVariable().name : null);
        List<Parser.ParameterRule> rules = rule.getParams();
        List<Entity> parameters = new ArrayList<>();

        for (var r : rules) {
            visitParameterRule(r);
            parameters.add(context.exposeBuffer());
        }

        call.parameters = parameters;
        context.addBuffer(context.executeFunction(call));

        rule.visited = true;
    }

    @Override
    public void visitParameterRule(Parser.ParameterRule rule) throws UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException {
        if (rule.visited) {
            return;
        }

        if (rule.getVariable() != null) {
            context.addBuffer(context.variablesTable.getVariable(rule.getVariable().name));
        } else if (rule.getLiteral() != null) {
            visitLiteralRule(rule.getLiteral());
        } else {
            visitFunctionCallRule(rule.getFunction());
        }

        rule.visited = true;
    }

    @Override
    public void visitLiteralRule(Parser.LiteralRule rule) {
        if (rule.visited) {
            return;
        }

        if (rule.getInteger() != null) {
            context.addBuffer(new IntegerEntity(rule.getInteger().getValue()));
        } else {
            context.addBuffer(new StringEntity(rule.getString().getValue()));
        }

        rule.visited = true;
    }

    @Override
    public void visitIDENTIFIER(Parser.IDENTIFIER rule) {
        if (rule.visited) {
            return;
        }

        rule.visited = true;
    }

    @Override
    public void visitINTEGER(Parser.INTEGER rule) {
        if (rule.visited) {
            return;
        }

        rule.visited = true;
    }

    @Override
    public void visitSTRING(Parser.STRING rule) {
        if (rule.visited) {
            return;
        }

        rule.visited = true;
    }

    @Override
    public void visitDEF(Parser.DEF rule) {
        if (rule.visited) {
            return;
        }

        rule.visited = true;
    }

    @Override
    public void visitCOMMA(Parser.COMMA rule) {
        if (rule.visited) {
            return;
        }

        rule.visited = true;
    }

    @Override
    public void visitDOT(Parser.DOT rule) {
        if (rule.visited) {
            return;
        }

        rule.visited = true;
    }

    @Override
    public void visitASSIGN(Parser.ASSIGN rule) {
        if (rule.visited) {
            return;
        }

        rule.visited = true;
    }

    @Override
    public void visitSEMICOLON(Parser.SEMICOLON rule) {
        if (rule.visited) {
            return;
        }

        rule.visited = true;
    }

    @Override
    public void visitOPEN_PAREN(Parser.OPEN_PAREN rule) {
        if (rule.visited) {
            return;
        }

        rule.visited = true;
    }

    @Override
    public void visitCLOSE_PAREN(Parser.CLOSE_PAREN rule) {
        if (rule.visited) {
            return;
        }

        rule.visited = true;
    }

    @Override
    public void visitEOL(Parser.EOL rule) {
        if (rule.visited) {
            return;
        }

        rule.visited = true;
    }

    @Override
    public void visitO_BR(Parser.O_BR rule) {
        if (rule.visited) {
            return;
        }

        rule.visited = true;
    }

    @Override
    public void visitC_BR(Parser.C_BR rule) {
        if (rule.visited) {
            return;
        }

        rule.visited = true;
    }



}
