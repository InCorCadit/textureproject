package com.company.interpreter.entity;

import com.company.interpreter.entity.base.Entity;
import com.company.interpreter.exceptions.ParametersNumberException;
import com.company.interpreter.exceptions.UnexpectedParameterException;
import com.company.interpreter.exceptions.UnknownFunctionException;
import com.company.interpreter.util.CastUtils;

import java.awt.image.BufferedImage;
import java.util.List;

public abstract class PatternEntity extends Entity {

    protected BackgroundEntity background;
    protected int rotation;

    @Override
    public void callFunction(String funName, List<Entity> params) throws ParametersNumberException, UnexpectedParameterException, UnknownFunctionException {
        switch (funName) {
            case "bg" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (!CastUtils.canAssign(params.get(0), BackgroundEntity.class)) throw new UnexpectedParameterException(funName, 1, "Background", params.get(0).getClass().toString());

                bg((BackgroundEntity) params.get(0));
            }
            case "rotation" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (!CastUtils.canAssign(params.get(0), IntegerEntity.class)) throw new UnexpectedParameterException(funName, 1, "Integer", params.get(0).getClass().toString());

                setRotation((IntegerEntity) params.get(0));
            }
            default -> throw new UnknownFunctionException("Canvas", funName);
        }
    }

    private void bg(BackgroundEntity newBackground) {
        background = newBackground;
    }

    private void setRotation(IntegerEntity entity) { rotation = entity.value; }

    public abstract void applyToCanvas(BufferedImage img);
}
