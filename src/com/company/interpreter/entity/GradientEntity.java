package com.company.interpreter.entity;

import com.company.interpreter.entity.base.Entity;
import com.company.interpreter.exceptions.ParametersNumberException;
import com.company.interpreter.exceptions.UnexpectedParameterException;
import com.company.interpreter.exceptions.UnknownFunctionException;
import com.company.interpreter.function.FunctionCall;

import java.awt.*;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class GradientEntity extends BackgroundEntity {
    private final String[] possibleTypes = new String[] { "vertical", "horizontal" };
    public final String type;
    public final String color1;
    public final String color2;

    public GradientEntity(FunctionCall call) throws ParametersNumberException, UnexpectedParameterException {
        List<StringEntity> params = call.parameters.stream().map(p -> (StringEntity) p).collect(Collectors.toList());
        if (params.size() != 3) {
            throw new ParametersNumberException(call.name, 3, params.size());
        }

        type = params.get(0).value;
        color1 = params.get(1).value;
        color2 = params.get(2).value;

        if (Arrays.stream(possibleTypes).noneMatch(s -> s.equals(type))) {
            throw new UnexpectedParameterException(call.name, 1, possibleTypes, type);
        }

        if (!isColor(color1)) {
            throw new UnexpectedParameterException(call.name, 2, "Color", color1);
        }

        if (!isColor(color1)) {
            throw new UnexpectedParameterException(call.name, 3, "Color", color2);
        }

    }

    @Override
    public void callFunction(String funName, List<Entity> params) throws UnknownFunctionException {
        switch (funName) {
            default -> super.callFunction(funName, params);
        }
    }

    @Override
    public void applyToCanvas(BufferedImage img) {
        Graphics2D g2d = (Graphics2D) img.getGraphics();
        Color[] colors = new Color[] { toColor(color1), toColor(color2) };
        float[] fractions = new float[] { 0.35f, 0.65f };

        var gradient = new LinearGradientPaint(
                new Point2D.Float(0, 0),
                new Point2D.Float( type.equals("vertical") ? 0 : img.getWidth(), type.equals("horizontal") ? 0 : img.getHeight()),
                fractions,
                colors);

        g2d.setPaint(gradient);
        g2d.fillRect(0, 0, img.getWidth(), img.getHeight());
    }

    @Override
    public void applyToFigure(BufferedImage img, Polygon p) {
        Graphics2D g2d = (Graphics2D) img.getGraphics();
        Color[] colors = new Color[] { toColor(color1), toColor(color2) };
        float[] fractions = new float[] { 0.35f, 0.65f };

        var gradient = new LinearGradientPaint(
                new Point2D.Float((float) p.getBounds2D().getMinX(), (float) p.getBounds2D().getMinY()),
                new Point2D.Float((float) p.getBounds2D().getMaxX(), (float) p.getBounds2D().getMaxY()),
                fractions,
                colors);

        g2d.setPaint(gradient);
        g2d.fill(p);
    }


}
