package com.company.interpreter.entity;

import com.company.interpreter.entity.base.Entity;
import com.company.interpreter.exceptions.UnknownFunctionException;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.regex.Pattern;

public abstract class BackgroundEntity extends Entity {

    @Override
    public void callFunction(String funName, List<Entity> params) throws UnknownFunctionException {
        switch (funName) {
            default -> throw new UnknownFunctionException("Background", funName);
        }
    }

    public abstract void applyToCanvas(BufferedImage img);

    public abstract void applyToFigure(BufferedImage img, Polygon p);

    protected boolean isColor(String hex) {
        Pattern pattern = Pattern.compile("^([a-fA-F0-9]{6}|[a-fA-F0-9]{3})$");
        return pattern.matcher(hex).matches();
    }

    protected Color toColor(String hex) { return new Color(Integer.parseInt(hex, 16)); }
}
