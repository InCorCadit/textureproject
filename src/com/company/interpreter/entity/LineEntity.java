package com.company.interpreter.entity;

import com.company.interpreter.entity.base.Entity;
import com.company.interpreter.exceptions.ParametersNumberException;
import com.company.interpreter.exceptions.UnexpectedParameterException;
import com.company.interpreter.exceptions.UnknownFunctionException;
import com.company.interpreter.function.FunctionCall;
import com.company.interpreter.util.CastUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class LineEntity extends PatternEntity {
    private final String[] possibleOrientations = new String[] { "horizontal", "vertical" };


    private String orientation;
    private int width;
    private int distance;

    public LineEntity(FunctionCall call) throws ParametersNumberException, UnexpectedParameterException {
        List<StringEntity> params = call.parameters.stream().map(p -> (StringEntity) p).collect(Collectors.toList());
        if (params.size() != 1) throw new ParametersNumberException(call.name, 1, params.size());

        orientation = params.get(0).value;

        if (Arrays.stream(possibleOrientations).noneMatch(s -> s.equals(orientation))) throw new UnexpectedParameterException(call.name, 1, possibleOrientations, orientation);
    }

    @Override
    public void callFunction(String funName, List<Entity> params) throws ParametersNumberException, UnexpectedParameterException, UnknownFunctionException {
        switch (funName) {
            case "orientation" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (Arrays.stream(possibleOrientations).noneMatch(s -> s.equals(orientation))) throw new UnexpectedParameterException(funName, 1, possibleOrientations, orientation);
                setOrientation((StringEntity) params.get(0));
            }
            case "width" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (!CastUtils.canAssign(params.get(0), IntegerEntity.class)) throw new UnexpectedParameterException(funName, 1, "Integer", params.get(0).getClass().toString());

                setSize((IntegerEntity) params.get(0));
            }
            case "distance" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (!CastUtils.canAssign(params.get(0), IntegerEntity.class)) throw new UnexpectedParameterException(funName, 1, "Integer", params.get(0).getClass().toString());

                setDistance((IntegerEntity) params.get(0));
            }

            default -> super.callFunction(funName, params);
        }
    }

    @Override
    public void applyToCanvas(BufferedImage img) {
        Graphics2D g2d = (Graphics2D) img.getGraphics();

        int s = 0;
        int length = orientation.equals("horizontal") ? img.getHeight() : img.getWidth();
        do {
            s += distance;

            Polygon p = orientation.equals("horizontal") ? createHorizontal(s, img.getWidth()) : createVertical(s, img.getHeight());
            g2d.drawPolygon(p);
            background.applyToFigure(img, p);

            s += width;
        } while (s < length - distance);
    }

    private void setOrientation(StringEntity entity) { this.orientation = entity.value; }

    private void setSize(IntegerEntity entity) { this.width = entity.value; }

    private void setDistance(IntegerEntity entity) { this.distance = entity.value; }


    private Polygon createHorizontal(int s, int imgWidth) {
        int[] x = { 0, 0, imgWidth, imgWidth};
        int[] y = { s, s + width, s + width, s};
        return new Polygon(x, y, 4);
    }

    private Polygon createVertical(int s, int imgHeight) {
        int[] x = { s, s + width, s + width, s };
        int[] y = { 0, 0, imgHeight, imgHeight };
        return new Polygon(x, y, 4);
    }
}
