package com.company.interpreter.entity.base;

import com.company.interpreter.exceptions.ParametersNumberException;
import com.company.interpreter.exceptions.UnexpectedParameterException;
import com.company.interpreter.exceptions.UnknownFunctionException;
import com.company.interpreter.exceptions.ValueOutOfBoundsException;

import java.util.List;

public abstract class Entity {
    public abstract void callFunction(String funName, List<Entity> params) throws ParametersNumberException, UnexpectedParameterException, UnknownFunctionException, ValueOutOfBoundsException;
}
