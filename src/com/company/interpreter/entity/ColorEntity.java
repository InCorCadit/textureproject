package com.company.interpreter.entity;

import com.company.interpreter.entity.base.Entity;
import com.company.interpreter.exceptions.ParametersNumberException;
import com.company.interpreter.exceptions.UnexpectedParameterException;
import com.company.interpreter.exceptions.UnknownFunctionException;
import com.company.interpreter.function.FunctionCall;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.List;
import java.util.stream.Collectors;

public class ColorEntity extends BackgroundEntity {
    public final String color;

    public ColorEntity(FunctionCall call) throws ParametersNumberException, UnexpectedParameterException {
        List<StringEntity> params = call.parameters.stream().map(p -> (StringEntity) p).collect(Collectors.toList());
        if (params.size() != 1) {
            throw new ParametersNumberException(call.name, 1, params.size());
        }

        color = params.get(0).value;

        if (!isColor(color)) {
            throw new UnexpectedParameterException(call.name, 1, "Color", color);
        }
    }

    @Override
    public void callFunction(String funName, List<Entity> params) throws UnknownFunctionException {
        switch (funName) {
            default -> super.callFunction(funName, params);
        }
    }

    @Override
    public void applyToCanvas(BufferedImage img) {
        Graphics2D g2d = (Graphics2D) img.getGraphics();
        g2d.setBackground(toColor(color));
        g2d.clearRect(0, 0, img.getWidth(), img.getHeight());
    }

    @Override
    public void applyToFigure(BufferedImage img, Polygon p) {
        Graphics2D g2d = (Graphics2D) img.getGraphics();
        g2d.setColor(toColor(color));
        g2d.fill(p);
    }

    public static BackgroundEntity DefaultWhiteBackground() throws ParametersNumberException, UnexpectedParameterException {
        var fc = new FunctionCall("Color", null);
        fc.parameters.add(new StringEntity("ffffff"));
        return new ColorEntity(fc);
    }
}
