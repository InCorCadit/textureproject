package com.company.interpreter.entity;

import com.company.interpreter.entity.base.Entity;

import java.util.List;

public class IntegerEntity extends Entity {
    public int value;

    public IntegerEntity(int value) {
        this.value = value;
    }

    @Override
    public void callFunction(String funName, List<Entity> params) {

    }
}
