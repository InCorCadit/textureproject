package com.company.interpreter.entity;

import com.company.interpreter.entity.base.Entity;
import com.company.interpreter.exceptions.ParametersNumberException;
import com.company.interpreter.exceptions.UnexpectedParameterException;
import com.company.interpreter.exceptions.UnknownFunctionException;
import com.company.interpreter.function.FunctionCall;
import com.company.interpreter.util.CastUtils;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class FigureEntity extends PatternEntity {
    private final String[] possibleTypes = new String[] { "triangle", "square", "circle", "diamond", "pentagon" };

    public String type;
    private int size;
    private int verticalDistance;
    private int horizontalDistance;

    public FigureEntity(FunctionCall call) throws ParametersNumberException, UnexpectedParameterException {
        List<StringEntity> params = call.parameters.stream().map(p -> (StringEntity) p).collect(Collectors.toList());
        if (params.size() != 1) {
            throw new ParametersNumberException(call.name, 1, params.size());
        }

        type = params.get(0).value;

        if (Arrays.stream(possibleTypes).noneMatch(s -> s.equals(type))) {
            throw new UnexpectedParameterException(call.name, 1, possibleTypes, type);
        }

        size = 20;
        verticalDistance = 10;
        horizontalDistance = 10;
        background = ColorEntity.DefaultWhiteBackground();
    }

    @Override
    public void callFunction(String funName, List<Entity> params) throws ParametersNumberException, UnexpectedParameterException, UnknownFunctionException {
        switch (funName) {
            case "type" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (Arrays.stream(possibleTypes).noneMatch(s -> s.equals(type))) throw new UnexpectedParameterException(funName, 1, possibleTypes, type);

                setType((StringEntity) params.get(0));
            }
            case "size" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (!CastUtils.canAssign(params.get(0), IntegerEntity.class)) throw new UnexpectedParameterException(funName, 1, "Integer", params.get(0).getClass().toString());

                setSize((IntegerEntity) params.get(0));
            }
            case "vertical_distance" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (!CastUtils.canAssign(params.get(0), IntegerEntity.class)) throw new UnexpectedParameterException(funName, 1, "Integer", params.get(0).getClass().toString());

                setVerticalDistance((IntegerEntity) params.get(0));
            }
            case "horizontal_distance" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (!CastUtils.canAssign(params.get(0), IntegerEntity.class)) throw new UnexpectedParameterException(funName, 1, "Integer", params.get(0).getClass().toString());

                setHorizontalDistance((IntegerEntity) params.get(0));
            }
            default -> super.callFunction(funName, params);
        }
    }

    private void setType(StringEntity entity) { this.type = entity.value; }

    private void setSize(IntegerEntity entity) { this.size = entity.value; }

    private void setVerticalDistance(IntegerEntity entity) { this.verticalDistance = entity.value; }

    private void setHorizontalDistance(IntegerEntity entity) { this.horizontalDistance = entity.value; }

    @Override
    public void applyToCanvas(BufferedImage img) {
        Graphics2D g2d = (Graphics2D) img.getGraphics();

        int x = 0, y = 0;
        do {
            y += verticalDistance;
            do {
                x += horizontalDistance;

                Polygon p = switch (type) {
                    case "triangle" -> createTriangle(x, y);
                    case "square" ->  createSquare(x, y);
                    case "circle" -> createCircle(x, y);
                    case "diamond" -> createDiamond(x, y);
                    default -> createPentagon(x, y);
                };

                g2d.drawPolygon(p);

                background.applyToFigure(img, p);

                x+= size;
            } while (x < img.getWidth() - size);

            x = 0;
            y += size;
        } while (y < img.getHeight() - size);
    }

    private Polygon createTriangle(int startX, int startY) {
        int[] x = { startX, startX + size / 2, startX + size };
        int[] y = { startY, startY + size, startY };
        return new Polygon(x, y, 3);
    }

    private Polygon createSquare(int startX, int startY) {
        int[] x = { startX, startX ,startX + size, startX + size };
        int[] y = { startY, startY + size, startY + size, startY };
        return new Polygon(x, y, 4);
    }

    private Polygon createCircle(int startX, int startY) {
        int centerX = startX + size / 2;
        int centerY = startY + size / 2;
        var x = new int[size - 1];
        var y = new int[size - 1];
        double sectors = 2 * Math.PI / size;

        int index = 0;
        double angle;
        for (int i = 0; i < size; i+=2) {
            angle = i * sectors;
            x[index] = centerX + (int) (Math.cos(angle) / 2 * size);
            y[index] = centerY + (int) (Math.sin(angle) / 2 * size);
            index++;

            if (i != 0 && i != size - 1) {
                x[index] = centerX + (int) (Math.cos(angle) / 2 * size);
                y[index] = centerY + (int) (Math.sin(angle) / 2 * size);
                index++;
            }

        }
        return new Polygon(x , y, size - 1);
    }

    private Polygon createDiamond(int startX, int startY) {
        int[] x = { startX + size/2, startX, startX + size/2, startX + size };
        int[] y = { startY, startY + size/2, startY + size, startY + size/2 };
        return new Polygon(x, y, 4);
    }

    private Polygon createPentagon(int startX, int startY) {
        int[] x = { startX + size/2, startX, startX + (int) (size / 4), startX + (int) (size / 1.25), startX + size };
        int[] y = { startY, startY + (int) (size / 2.25), startY + size, startY + size, startY + (int) (size / 2.25) };
        return new Polygon(x, y, 5);
    }

}


