package com.company.interpreter.entity;

import com.company.interpreter.entity.base.Entity;

import java.util.List;

public class StringEntity extends Entity {
    public String value;

    public StringEntity(String value) {
        this.value = value;
    }

    @Override
    public void callFunction(String funName, List<Entity> params) {

    }
}
