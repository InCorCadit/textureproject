package com.company.interpreter.entity;

import com.company.interpreter.entity.base.Entity;
import com.company.interpreter.exceptions.ParametersNumberException;
import com.company.interpreter.exceptions.UnexpectedParameterException;
import com.company.interpreter.exceptions.UnknownFunctionException;
import com.company.interpreter.exceptions.ValueOutOfBoundsException;
import com.company.interpreter.function.FunctionCall;
import com.company.interpreter.util.CastUtils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CanvasEntity extends Entity {
    private BufferedImage canvas;

    private BackgroundEntity background;
    private List<PatternEntity> patterns;
    private int blur;
    private int brightness;
    private boolean isBlackWhite;

    public CanvasEntity(FunctionCall call) throws ClassCastException, ParametersNumberException, UnexpectedParameterException {
        List<IntegerEntity> params = call.parameters.stream().map(p -> (IntegerEntity) p).collect(Collectors.toList());
        if (params.size() != 2) {
            throw new ParametersNumberException(call.name, 2, params.size());
        }

        canvas = new BufferedImage(params.get(0).value, params.get(1).value, BufferedImage.TYPE_INT_ARGB);
        patterns = new ArrayList<>();
        background = ColorEntity.DefaultWhiteBackground();
        blur = 0;
        brightness = 50;
        isBlackWhite = false;
    }

    @Override
    public void callFunction(String funName, List<Entity> params) throws ParametersNumberException, UnexpectedParameterException, UnknownFunctionException, ValueOutOfBoundsException {
        switch (funName) {
            case "bg" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (!CastUtils.canAssign(params.get(0), BackgroundEntity.class)) throw new UnexpectedParameterException(funName, 1, "Background", params.get(0).getClass().toString());

                bg((BackgroundEntity) params.get(0));
            }
            case "add_pattern" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (!CastUtils.canAssign(params.get(0), PatternEntity.class)) throw new UnexpectedParameterException(funName, 1, "Pattern", params.get(0).getClass().toString());

                addPattern((PatternEntity) params.get(0));
            }
            case "remove_pattern" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (!CastUtils.canAssign(params.get(0), PatternEntity.class) && !CastUtils.canAssign(params.get(0), IntegerEntity.class)) throw new UnexpectedParameterException(funName, 1, "Pattern or Integer", params.get(0).getClass().toString());

                if (CastUtils.canAssign(params.get(0), PatternEntity.class)) removePattern((PatternEntity) params.get(0));
                if (CastUtils.canAssign(params.get(0), IntegerEntity.class)) {
                    IntegerEntity entity = (IntegerEntity) params.get(0);
                    if (entity.value < 0 || entity.value > patterns.size() - 1) throw new ValueOutOfBoundsException(funName, 1, 0, patterns.size() - 1, entity.value);
                    removePattern(entity);
                }
            }
            case "blur" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (!CastUtils.canAssign(params.get(0), IntegerEntity.class)) throw new UnexpectedParameterException(funName, 1, "Pattern", params.get(0).getClass().toString());

                IntegerEntity entity = (IntegerEntity) params.get(0);
                if (entity.value < 0 || entity.value > 100) throw new ValueOutOfBoundsException(funName, 1, 0, 100, entity.value);
                setBlur(entity);
            }
            case "brightness" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (!CastUtils.canAssign(params.get(0), IntegerEntity.class)) throw new UnexpectedParameterException(funName, 1, "Pattern", params.get(0).getClass().toString());

                IntegerEntity entity = (IntegerEntity) params.get(0);
                if (entity.value < 0 || entity.value > 100) throw new ValueOutOfBoundsException(funName, 1, -50, 50, entity.value);
                setBrightness(entity);
            }
            case "black_white" -> {
                if (params.size() != 1) throw new ParametersNumberException(funName, 1, params.size());
                if (!CastUtils.canAssign(params.get(0), IntegerEntity.class)) throw new UnexpectedParameterException(funName, 1, "Pattern", params.get(0).getClass().toString());

                IntegerEntity entity = (IntegerEntity) params.get(0);
                if (entity.value < 0 || entity.value > 1) throw new ValueOutOfBoundsException(funName, 1, 0, 1, entity.value);
                setBlackWhite(entity);
            }
            default -> throw new UnknownFunctionException("Canvas", funName);
        }
    }

    private void bg(BackgroundEntity newBackground) {
        background = newBackground;
    }

    private void addPattern(PatternEntity pattern) {
        patterns.add(pattern);
    }

    private void removePattern(PatternEntity pattern) {
        patterns.remove(pattern);
    }

    private void removePattern(IntegerEntity pos) {
        patterns.remove(pos.value);
    }

    private void setBlur(IntegerEntity entity) {
        blur = entity.value;
    }

    private void setBrightness(IntegerEntity entity) {
        brightness = entity.value;
    }

    private void setBlackWhite(IntegerEntity entity) {
        isBlackWhite = entity.value == 1;
    }

    public void saveSelf(String fileName) throws IOException {
        background.applyToCanvas(canvas);

        for (PatternEntity pattern : patterns) {
            pattern.applyToCanvas(canvas);
        }

        if (blur != 0)
            applyBlur();

        if (brightness != 50)
            applyBrightness();

        if (isBlackWhite) {
            applyBlackWhite();
        }

        canvas.getGraphics().dispose();

        File output = new File(fileName + ".png");
        ImageIO.write(canvas, "png", output);
    }

    private void applyBlur() {
        for (int blurFactor = 1; blurFactor < blur / 2; blurFactor += Math.max(blur / 10, 3)) {
            float[] blurKernel = new float[blurFactor * blurFactor];
            Arrays.fill(blurKernel, 1f / (blurFactor * blurFactor));

            BufferedImageOp blurOp = new ConvolveOp(new Kernel(blurFactor, blurFactor, blurKernel), ConvolveOp.EDGE_NO_OP, null);
            canvas = blurOp.filter(canvas, null);
        }
    }

    private void applyBrightness() {
        float scaleFactor = brightness > 50 ? 1f : brightness / 50f;
        float offset = brightness > 50 ? brightness - 50 : 0;
        RescaleOp rescaleOp = new RescaleOp(scaleFactor, offset, null);
        canvas = rescaleOp.filter(canvas, null);
    }

    private void applyBlackWhite() {
        var result = new BufferedImage(canvas.getWidth(), canvas.getHeight(), BufferedImage.TYPE_BYTE_BINARY);

        result.getGraphics().drawImage(canvas, 0, 0, Color.WHITE, null);
        result.getGraphics().dispose();

        canvas = result;
    }

}
