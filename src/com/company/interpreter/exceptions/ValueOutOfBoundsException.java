package com.company.interpreter.exceptions;

public class ValueOutOfBoundsException extends Exception{
    public ValueOutOfBoundsException(String funName, int pos, int min, int max, int actual) {
        super(String.format("Value at position %d of function %s should be between %s and %s. Actual value is %s", pos, funName, min, max, actual));
    }
}
