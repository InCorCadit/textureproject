package com.company.interpreter.exceptions;

public class ParametersNumberException extends Exception {
    public ParametersNumberException(String identifier, int expected, int actual) {
        super(String.format("Function %s expected %d parameters, but got %d: ", identifier, expected, actual));
    }
}
