package com.company.interpreter.exceptions;

public class UnexpectedParameterException extends Exception {
    public UnexpectedParameterException(String function, int position, String[] expected, String actual) {
        super(String.format("Function %s is expecting one of the following values at position %d: \n", function, position) +
                String.join(", ", expected) +
                String.format("; received \"%s\"", actual));
    }

    public UnexpectedParameterException(String function, int position, String expected, String actual) {
        super(String.format("Function %s is expecting a %s at position %d, but received \"%s\"", function, expected, position, actual));
    }
}
