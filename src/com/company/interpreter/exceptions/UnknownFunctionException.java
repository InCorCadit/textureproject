package com.company.interpreter.exceptions;

public class UnknownFunctionException extends Exception {
    public UnknownFunctionException(String typeName, String func) {
        super(String.format("Type %s has no function \"%s\" ", typeName, func));
    }
}
