package com.company.interpreter.exceptions;

public class DuplicateIdentifierException extends Exception {

    public DuplicateIdentifierException(String identifier) {
        super("Duplicate Identifier: " + identifier);
    }
}
