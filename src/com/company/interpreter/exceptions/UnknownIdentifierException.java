package com.company.interpreter.exceptions;

public class UnknownIdentifierException extends Exception {
    public UnknownIdentifierException(String identifier) {
        super("Unknown identifier: " + identifier);
    }
}
