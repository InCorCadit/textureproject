package com.company.interpreter.function;

import com.company.interpreter.entity.base.Entity;

import java.util.ArrayList;
import java.util.List;

public class FunctionCall {
    public String variableName;
    public final String name;
    public List<Entity> parameters;

    public FunctionCall(String name, String variableName) {
        this.variableName = variableName;
        this.name = name;
        this.parameters = new ArrayList<>();
    }
}
