package com.company.interpreter;

import com.company.interpreter.definition.VariablesTable;
import com.company.interpreter.entity.*;
import com.company.interpreter.entity.base.Entity;
import com.company.interpreter.exceptions.*;
import com.company.interpreter.function.FunctionCall;
import com.company.interpreter.util.CastUtils;

import java.io.IOException;
import java.util.Stack;

public class Context {
    VariablesTable variablesTable;
    Stack<FunctionCall> functionCalls;
    private Entity buffer;


    public Context() {
        variablesTable = new VariablesTable();
        functionCalls = new Stack<>();
    }

    public void addBuffer(Entity buffer) {
        this.buffer = buffer;
    }

    public Entity exposeBuffer() {
        Entity value = buffer;
        buffer = null;
        return value;
    }

    public Entity executeFunction(FunctionCall call) throws UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException {
        if (call.variableName != null) {
            Entity callingEntity = variablesTable.getVariable(call.variableName);
            callingEntity.callFunction(call.name, call.parameters);
        } else {
            return builtInFunction(call);
        }

        return null;
    }

    private Entity builtInFunction(FunctionCall call) throws UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, IOException {
        return switch (call.name) {
            case "Canvas" -> new CanvasEntity(call);
            case "Figure" -> new FigureEntity(call);
            case "Line" -> new LineEntity(call);
            case "Color" -> new ColorEntity(call);
            case "Gradient" -> new GradientEntity(call);
            case "save" -> saveImage(call);
            default -> throw new UnknownIdentifierException(call.name);
        };
    }

    private Entity saveImage(FunctionCall call) throws ParametersNumberException, UnexpectedParameterException, IOException {
        if (call.parameters.size() != 2)
            throw new ParametersNumberException("save", 2, call.parameters.size());

        if (!CastUtils.canAssign(call.parameters.get(0), CanvasEntity.class))
            throw new UnexpectedParameterException("save", 1, CanvasEntity.class.getSimpleName(), call.parameters.get(0).getClass().getSimpleName());

        if (!CastUtils.canAssign(call.parameters.get(1), StringEntity.class))
            throw new UnexpectedParameterException("save", 2, CanvasEntity.class.getSimpleName(), call.parameters.get(1).getClass().getSimpleName());


        CanvasEntity canvas = (CanvasEntity) call.parameters.get(0);
        StringEntity name = (StringEntity) call.parameters.get(1);

        canvas.saveSelf(name.value);

        return canvas;
    }



}
