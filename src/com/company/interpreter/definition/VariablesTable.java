package com.company.interpreter.definition;

import com.company.interpreter.entity.base.Entity;
import com.company.interpreter.exceptions.DuplicateIdentifierException;
import com.company.interpreter.exceptions.UnknownIdentifierException;

import java.util.HashMap;

public class VariablesTable {
    private final HashMap<String, Entity> variables;

    public VariablesTable() {
        this.variables = new HashMap<>();
    }


    public void addVariable(String name, Entity entity) throws DuplicateIdentifierException {
        if (variables.containsKey(name)) {
            throw new DuplicateIdentifierException(name);
        }

        variables.put(name, entity);
    }

    public Entity getVariable(String name) throws UnknownIdentifierException {
        if (!variables.containsKey(name)) {
            throw new UnknownIdentifierException(name);
        }

        return variables.get(name);
    }
}
