package com.company.visitor;

import com.company.interpreter.exceptions.*;
import com.company.parser.Parser;

import java.io.IOException;

public interface Visitor {

    void visitSourceCodeRule(Parser.SourceCodeRule rule);

    void visitDefinitionRule(Parser.DefinitionRule rule) throws DuplicateIdentifierException, UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException;

    void visitFunctionCallRule(Parser.FunctionCallRule rule) throws UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException;

    void visitParameterRule(Parser.ParameterRule rule) throws UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException;

    void visitLiteralRule(Parser.LiteralRule rule);

    void visitIDENTIFIER(Parser.IDENTIFIER rule);

    void visitINTEGER(Parser.INTEGER rule);

    void visitSTRING(Parser.STRING rule);

    void visitDEF(Parser.DEF rule);

    void visitCOMMA(Parser.COMMA rule);

    void visitDOT(Parser.DOT rule);

    void visitASSIGN(Parser.ASSIGN rule);

    void visitSEMICOLON(Parser.SEMICOLON rule);

    void visitOPEN_PAREN(Parser.OPEN_PAREN rule);

    void visitCLOSE_PAREN(Parser.CLOSE_PAREN rule);

    void visitEOL(Parser.EOL rule);

    void visitO_BR(Parser.O_BR rule);
    void visitC_BR(Parser.C_BR rule);
}
