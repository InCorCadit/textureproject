package com.company.visitor;

import com.company.parser.Parser;

public class PrintVisitor extends BaseVisitor {
    @Override
    public void visitSourceCodeRule(Parser.SourceCodeRule rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitDefinitionRule(Parser.DefinitionRule rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitFunctionCallRule(Parser.FunctionCallRule rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitParameterRule(Parser.ParameterRule rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitLiteralRule(Parser.LiteralRule rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitIDENTIFIER(Parser.IDENTIFIER rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitINTEGER(Parser.INTEGER rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitSTRING(Parser.STRING rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitDEF(Parser.DEF rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitCOMMA(Parser.COMMA rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitDOT(Parser.DOT rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitASSIGN(Parser.ASSIGN rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitSEMICOLON(Parser.SEMICOLON rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitOPEN_PAREN(Parser.OPEN_PAREN rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitCLOSE_PAREN(Parser.CLOSE_PAREN rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitEOL(Parser.EOL rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitO_BR(Parser.O_BR rule) {
        System.out.print(rule.getClass().getSimpleName());
    }

    @Override
    public void visitC_BR(Parser.C_BR rule) {
        System.out.print(rule.getClass().getSimpleName());
    }
}
