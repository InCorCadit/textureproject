package com.company.visitor;

import com.company.interpreter.exceptions.*;
import com.company.parser.Parser;

import java.io.IOException;

public class BaseVisitor implements Visitor {

    @Override
    public void visitSourceCodeRule(Parser.SourceCodeRule rule) {

    }

    @Override
    public void visitDefinitionRule(Parser.DefinitionRule rule) throws DuplicateIdentifierException, UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException {

    }

    @Override
    public void visitFunctionCallRule(Parser.FunctionCallRule rule) throws UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException {

    }

    @Override
    public void visitParameterRule(Parser.ParameterRule rule) throws UnknownIdentifierException, ParametersNumberException, UnexpectedParameterException, ValueOutOfBoundsException, UnknownFunctionException, IOException {

    }

    @Override
    public void visitLiteralRule(Parser.LiteralRule rule) {

    }

    @Override
    public void visitIDENTIFIER(Parser.IDENTIFIER rule) {

    }

    @Override
    public void visitINTEGER(Parser.INTEGER rule) {

    }

    @Override
    public void visitSTRING(Parser.STRING rule) {

    }

    @Override
    public void visitDEF(Parser.DEF rule) {

    }

    @Override
    public void visitCOMMA(Parser.COMMA rule) {

    }

    @Override
    public void visitDOT(Parser.DOT rule) {

    }

    @Override
    public void visitASSIGN(Parser.ASSIGN rule) {

    }

    @Override
    public void visitSEMICOLON(Parser.SEMICOLON rule) {

    }

    @Override
    public void visitOPEN_PAREN(Parser.OPEN_PAREN rule) {

    }

    @Override
    public void visitCLOSE_PAREN(Parser.CLOSE_PAREN rule) {

    }

    @Override
    public void visitEOL(Parser.EOL rule) {

    }

    @Override
    public void visitO_BR(Parser.O_BR rule) {

    }

    @Override
    public void visitC_BR(Parser.C_BR rule) {

    }
}
