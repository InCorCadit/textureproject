def angled = Canvas:(1060, 500);
angled.bg:(Color:('b6afd0'));
angled.brightness:(30);

def pg = Figure:('pentagon');
pg.size:(120);
pg.vertical_distance:(40);
pg.horizontal_distance:(50);
pg.bg:(Gradient:('horizontal', '4420c5', '7060a9'));

def dm = Figure:('diamond');
dm.size:(50);
dm.vertical_distance:(110);
dm.horizontal_distance:(120);
dm.bg:(Gradient:('vertical', '0bcb18', '4cf657'));

angled.add_pattern:(pg);
angled.add_pattern:(dm);

save:(angled, 'angled');


def round = Canvas:(800, 800);
round.bg:(Color:('a12a42'));
round.blur:(30);

def c = Gradient:('vertical', '9800b3', '0600b3');

def center = Figure:('circle');
center.size:(450);
center.vertical_distance:(175);
center.horizontal_distance:(175);
center.bg:(c);

def around1 = Figure:('circle');
around1.size:(80);
around1.vertical_distance:(45);
around1.horizontal_distance:(360);
around1.bg:(c);

def around2 = Figure:('circle');
around2.size:(80);
around2.vertical_distance:(360);
around2.horizontal_distance:(45);
around2.bg:(c);

def around3 = Figure:('circle');
around3.size:(80);
around3.vertical_distance:(140);
around3.horizontal_distance:(140);
around3.bg:(c);

def around4 = Figure:('circle');
around4.size:(80);
around4.vertical_distance:(360);
around4.horizontal_distance:(140);
around4.bg:(Color:('a12a42'));


def around5 = Figure:('circle');
around5.size:(80);
around5.vertical_distance:(140);
around5.horizontal_distance:(360);
around5.bg:(Color:('a12a42'));


round.add_pattern:(around1);
round.add_pattern:(around2);
round.add_pattern:(around3);
round.add_pattern:(around4);
round.add_pattern:(around5);
round.add_pattern:(center);

save:(round, 'round');



